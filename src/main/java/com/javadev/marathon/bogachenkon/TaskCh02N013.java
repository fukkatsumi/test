package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите число: ");
        int number = s.nextInt();
        System.out.println("Перевернутое число: " + readNumberFromRightToLeft(number));
    }

    public static int readNumberFromRightToLeft(int aNumber) {
        int result1 = aNumber % 10;
        aNumber = aNumber / 10;
        int result2 = aNumber % 10;
        aNumber = aNumber / 10;
        int result3 = aNumber % 10;
        return ((result1 * 100) + (result2 * 10) + (result3));
    }
}
