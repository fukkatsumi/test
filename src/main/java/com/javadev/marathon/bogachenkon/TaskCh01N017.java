package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh01N017 {
    public static void main(String[] args) {
        inputNumber();
    }

    public static void inputNumber() {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите число x: ");
        Double x = s.nextDouble();
        System.out.println("Введите число a: ");
        Double a = s.nextDouble();
        System.out.println("Введите число b: ");
        Double b = s.nextDouble();
        System.out.println("Введите число c: ");
        Double c = s.nextDouble();

        System.out.println("exerciseO: " + exerciseO(x)
                + "\n exerciseP: " + exerciseP(x)
                + "\n exerciseR: " + exerciseR(x, a, b, c)
                + "\n exerciseS: " + exerciseS(x));
    }

    public static Double exerciseO(Double x) {
        return (Math.sqrt(1 - Math.pow(Math.sin(x), 2)));
    }

    public static Double exerciseP(Double x) {
        return (((Math.sqrt(x + 1)) + (Math.sqrt(x - 1))) / (2 * Math.sqrt(x)));
    }

    public static Double exerciseR(Double x, Double a, Double b, Double c) {
        return (1 / Math.sqrt(a * Math.pow(x, 2) + b * x + c));
    }

    public static Double exerciseS(Double x) {
        return (Math.abs(x) + Math.abs(x + 1));
    }
}
