package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh01N003 {
    public static void main(String[] args) {
        showNumberOnScreen();
    }

    public static void showNumberOnScreen() {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите число: ");
        System.out.println("Вы ввели число " + s.nextInt());
    }
}
