package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите число: ");
        int number = s.nextInt();
        System.out.println("Исходное число: " + findDefaultNumber(number));
    }

    public static int findDefaultNumber(int aNumber) {
        int result1 = aNumber % 10;
        aNumber = aNumber / 10;
        int result2 = aNumber % 10;
        aNumber = aNumber / 10;
        int result3 = aNumber % 10;
        return ((result3 * 100) + (result1 * 10) + (result2));
    }
}
