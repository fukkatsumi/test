package com.javadev.marathon.bogachenkon;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите два числа через Enter: ");
        int a = s.nextInt();
        int b = s.nextInt();
        System.out.println("Результат проверки на делимость" + divisibilityCheck(a, b));
    }

    public static int divisibilityCheck(int firstNumber, int secondNumber) {
        return ((firstNumber % secondNumber) * (secondNumber % firstNumber) + 1);
    }
}
